#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "boards.h"
#include "nrf_drv_spi.h"
#include "lis3dsh_drive.h"
// Must include these headers to work with nrf logger
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf.h"
#include "nrf_uarte.h"

#define UART_TX_BUFF_SIZE 128
#define UART_RX_BUFF_SIZE 128

// This event handler will be called whenever a uart event occurs
void uart_event_handler(app_uart_evt_t * p_event)
{

// create a varibale which will hold the characters that we are going to read over uart
  uint8_t c;


// check if some communication error occured, this event will be trigered
  if(p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
  {
	// Check the reason of error by accessing the error variable and then pass it to error handler
    APP_ERROR_HANDLER(p_event->data.error_communication);
  }
  
// check if fifo error occured, this might be fifo overflow or other relevant errors  
  else if (p_event->evt_type == APP_UART_FIFO_ERROR)
  {
	// Check the reason of error by accessing the error variable and then pass it to error handler  
    APP_ERROR_HANDLER(p_event->data.error_code);
  }
// check if there is a character available to be read from the buffer
  else if (p_event->evt_type == APP_UART_DATA_READY)
  {
	// Read the available character and store it in the variable  
    app_uart_get(&c);

// print a message over uart port along with the character information
    printf("The char received over UART is: %c \r\n", c);

  }

// check if the transmission over uart port is finished i.e. uart tx empty event was generated
  else if (p_event->evt_type == APP_UART_TX_EMPTY)
  {
	// toggle an led or use any other function here   
    //nrf_gpio_pin_toggle(led);
  }


}

// A funtion that will configure the UART settings
void uart_config(void)
{
  uint32_t err_code; // a variable to hold error value

  const app_uart_comm_params_t comm_params =  // a struct that will hold all the uart configurations
  {
      RX_PIN_NUMBER, // Mention the Pin number for RX pin
      TX_PIN_NUMBER, // Mention the Pin number for TX pin
      RTS_PIN_NUMBER, // Mention the Pin number for RTS pin
      CTS_PIN_NUMBER, // Mention the Pin number for CTS pin 
      APP_UART_FLOW_CONTROL_DISABLED, // Disable the hardware flow control, we only need it if we are using high baud rates, we can save some pins by disabling this
      false, // event parity if true, if false no parity 
      NRF_UARTE_BAUDRATE_115200 // make sure to write NRF_UARTE_BAUDRATE_115200 and not the NRF_UART_BAUDRATE_115200
  };


// initialize the uart with the fifo and pass it the parameters
// Parameters = (configurations, rx buffer size, tx buffer size, event handler, interrupt priority, error code variable) 
APP_UART_FIFO_INIT(&comm_params, UART_RX_BUFF_SIZE, UART_TX_BUFF_SIZE, uart_event_handler, APP_IRQ_PRIORITY_LOWEST, err_code);
APP_ERROR_CHECK(err_code); // check if some error occured during initialization

}



int main(void)
{


    // Initialize the Logger module and check if any error occured during initialization
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
	
    // Initialize the default backends for nrf logger
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    

    // print the log msg over uart port
    printf("This is log data from nordic device..");


    // variables to hold x y z values in mg
    int intValueMgV, intValueMgI;
	
    
	
	
    //Call the SPI initialization function
    SPI_Init();
    nrf_delay_ms(500);
    
    // Call the CS5463 initialization function
    CS5463_init();
  uart_config();
 		
    while(true)
      {
		//
		intValueMgV = ((CS5463_read_reg(ADD_REG_VRMS) << 8) /*| CS5463_read_reg(ADD_REG_OUT_X_L)*/);
		intValueMgI = ((CS5463_read_reg(ADD_REG_IRMS) << 8) /*| CS5463_read_reg(ADD_REG_OUT_Y_L)*/);
		
		/* transform X value from two's complement to 16-bit int */
		intValueMgV = twoComplToInt16(intValueMgV);
		/* convert X absolute value to mg value */
		intValueMgV = intValueMgV * SENS_2G_RANGE_MG_PER_DIGIT;

		/* transform Y value from two's complement to 16-bit int */
		intValueMgI = twoComplToInt16(intValueMgI);
		/* convert Y absolute value to mg value */
		intValueMgI = intValueMgI * SENS_2G_RANGE_MG_PER_DIGIT;

		printf("X=%6d Y=%6d  \r\n", intValueMgV, intValueMgI/*,intValueMgZ*/);
		nrf_delay_ms(300);
		
      }
}   

