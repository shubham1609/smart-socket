#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "boards.h"
#include "app_uart.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "app_util_platform.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

/* Create an empty handler and pass this handler in the saadc initialization function
  > Normally this handler deals with the adc events but we are using blocking mode
  > In blocking mode the functions are called and the processor waits for the adc to finish taking samples from the respective channels
  > Event handler will not be called in this method*/

#define MAX_TEST_DATA_BYTES     (15U)                /**< max number of test bytes to be used for tx and rx. */
#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256                         /**< UART RX buffer size. */

#define led 12

void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}
// This event handler will be called whenever a uart event occurs
void uart_event_handler(app_uart_evt_t * p_event)
{

//// create a varibale which will hold the characters that we are going to read over uart
  uint8_t c;


//// check if some communication error occured, this event will be trigered
  if(p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
  {
//	// Check the reason of error by accessing the error variable and then pass it to error handler
    APP_ERROR_HANDLER(p_event->data.error_communication);
  }
  
//// check if fifo error occured, this might be fifo overflow or other relevant errors  
  else if (p_event->evt_type == APP_UART_FIFO_ERROR)
  {
	// Check the reason of error by accessing the error variable and then pass it to error handler  
    APP_ERROR_HANDLER(p_event->data.error_code);
  }
//// check if there is a character available to be read from the buffer
  else if (p_event->evt_type == APP_UART_DATA_READY)
  {
	
  }
}

/* When UART is used for communication with the host do not use flow control.*/
#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED



void saadc_callback_handler(nrf_drv_saadc_evt_t const * p_event)
{
 // Empty handler function
}




// Create a function which configures the adc input pins and channels as well as the mode of operation of adc

void saadc_init(void)
{
	// A variable to hold the error code
  ret_code_t err_code;

  // Create a config struct and assign it default values along with the Pin number for ADC Input
  // Configure the input as Single Ended(One Pin Reading)
  // Make sure you allocate the right pin.
  nrf_saadc_channel_config_t channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);

  // Initialize the saadc 
  // first parameter is for configuring the adc resolution and other features, we will see in future tutorial
  //on how to work with it. right now just pass a simple null value
  err_code = nrf_drv_saadc_init(NULL, saadc_callback_handler);
  APP_ERROR_CHECK(err_code);

// Initialize the Channel which will be connected to that specific pin.
  err_code = nrfx_saadc_channel_init(0, &channel_config);
  APP_ERROR_CHECK(err_code);

}


/**
 * @brief Function for main application entry.
 */
void uart_config(void)
{
   uint32_t err_code; // a variable to hold error value
  const app_uart_comm_params_t comm_params =  // a struct that will hold all the uart configurations
  {
      RX_PIN_NUMBER, // Mention the Pin number for RX pin
      TX_PIN_NUMBER, // Mention the Pin number for TX pin
      //RTS_PIN_NUMBER, // Mention the Pin number for RTS pin
      //CTS_PIN_NUMBER, // Mention the Pin number for CTS pin 
      APP_UART_FLOW_CONTROL_DISABLED, // Disable the hardware flow control, we only need it if we are using high baud rates, we can save some pins by disabling this
      false, // event parity if true, if false no parity 
      NRF_UARTE_BAUDRATE_115200 // make sure to write NRF_UARTE_BAUDRATE_115200 and not the NRF_UART_BAUDRATE_115200
  };

 APP_UART_FIFO_INIT(&comm_params, UART_RX_BUF_SIZE, UART_TX_BUF_SIZE, uart_event_handler, APP_IRQ_PRIORITY_LOWEST, err_code);
 APP_ERROR_CHECK(err_code); // check if some error occured during initialization
}
 
int main(void)
{
 
   int i;
 for(i=0;i<=10;i++){
  nrf_gpio_cfg_output(led);
   nrf_gpio_pin_clear(led);
   nrf_gpio_pin_set(led);
   nrf_delay_ms(200);
   nrf_gpio_pin_clear(led);
   nrf_delay_ms(200);
   }

  uart_config();
// call the saadc initialization function created above
  saadc_init();

  
// a struct to hold 16-bit value, create a variable of this type because our input resolution may vary from 8 bit to 14 bits depending on our configurations
// this variable holds the adc sample value
  nrf_saadc_value_t adc_val;

  printf("\r\nUART example started.\r\n");

// Print a simple msg that everything started without any error
  printf("Application Started!!!");


   
// Inifinite loop
    while (1)
    {
  
		// a blocking function which will be called and the processor waits until the value is read                     
		// the sample value read is in 2's complement and is automatically converted once retrieved
		// first parameter is for the adc input channel 
		// second parameter is to pass the address of the variable in which we store our adc sample value
      nrfx_saadc_sample_convert(0, &adc_val);
      printf("hello pc\r\n");
		// print this value using nrf log : here %d represents the integer value 
      printf("Sample value Read: %d\r\n", adc_val);
		
		// use nrf log and float marker to show the floating point values on the log
		// calculate the voltage by this: input_sample * 3.6 / 2^n (where n = 8 or 10 or 12 or 14 depending on our configuration for resolution in bits)
      printf("Volts: " NRF_LOG_FLOAT_MARKER "\r\n", NRF_LOG_FLOAT(adc_val * 3.6 / 512));


       
	   // give 500ms delay 
       nrf_delay_ms(500);
        
    }       
}


/** @} */